package tw.davidlee.demo.microservice.producer.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tw.davidlee.demo.microservice.producer.service.RedisCacheService;
import tw.davidlee.starter.security.utils.RedisUtil;

@Slf4j
@RestController
@RequestMapping("/api/v1/redis")
@Api(tags ="Redis 操作")
public class RedisController {

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private RedisCacheService redisCacheService;

    /**
     *    @Cacheable
     *    当重复使用 "相同参数" 调用方法的时候，方法本身不会被调用执行，直接从缓存中找到并返回
     */
    @GetMapping(value = "/cach/{id}/{name}" , produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation( value= "缓存測試 , 当重复使用'相同参数'")
    public Object testCacheable(@PathVariable String id , @PathVariable String name) {
        return redisCacheService.getById(id,name);
    }


    /**
     *    redissonClient塞值測試
     *    參考網址: https://www.jianshu.com/p/3f3c3c733f32
     */
//    @GetMapping(value = "/redissonClient/{key}/{value}" , produces = MediaType.APPLICATION_JSON_VALUE )
//    public String testRedissonClient(@PathVariable String key , @PathVariable String value) {
        // 字符串示例:
        //首先獲取redis中的key-value物件，key不存在沒關係
//        RBucket<String> keyObject = redisUtil.getBucket("auth-"+key);
//        如果key不存在，v=null
//        String v = keyObject.get();
//        設定key的值
//        keyObject.set("中文");
//        RBucket<String> keyObject2 = dataRedissonClient.getBucket("data-"+key);
        //如果key存在，就設定key的值為新值value
        //如果key不存在，就設定key的值為value
        //keyObject2.set(value);
//        return "1";
//
//    }


    /**
     *    redissonClient  lock測試
     *    redisson會自動為鎖增加expire時間 , 並監測取得鎖的線程是否還在執行 ,
     *    還在執行則延遲超時時間 , 其他線程沒有取得鎖則循環嘗試加鎖
     *    需要事先在redis增加stock紅包數
     *
     */
    @GetMapping(value = "/redissonClient/lock" , produces = "application/json;charset=utf-8")
    public String testRedissonLock() {

        // 分布式鎖key名稱
        String lockKey = "lock";
        try{
            // 上鎖
            redisUtil.lock(lockKey);
            // 紅包數-1
//            RBucket<String> keyObject = redissonClient.getBucket("stock");
//            int stock = Integer.parseInt(keyObject.get());
//            if(stock>0){
//                keyObject.set( String.valueOf(stock-1));
//                System.out.println("扣減成功 , 庫存剩餘:"+(stock-1));
//            }else{
//                System.out.println("扣減不足");
//            }
        }finally {
            // 解鎖
            redisUtil.unLock(lockKey);
        }



        return "1";

    }
}
