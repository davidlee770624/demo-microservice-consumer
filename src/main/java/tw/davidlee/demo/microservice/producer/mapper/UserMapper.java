package tw.davidlee.demo.microservice.producer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import tw.davidlee.demo.microservice.producer.entity.User;
import tw.davidlee.demo.microservice.producer.sqlprovider.UserSqlProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

/**
 * MyBatis-Plus增加常用CRUD操作
 * **/
@Mapper
public interface UserMapper extends BaseMapper<User> {

    @SelectProvider(type = UserSqlProvider.class, method = "selectAll")
    List<User> selectAll();

    @SelectProvider(type = UserSqlProvider.class, method = "query")
    List<User> query(Page<User> page);

}
