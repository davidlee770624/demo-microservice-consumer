package tw.davidlee.demo.microservice.producer.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import tw.davidlee.microservice.springmvc.bo.QueryPageBO;

@Data
@ApiModel(description = "查詢人員基本資料")
public class QueryUsersBO extends QueryPageBO {
    @ApiModelProperty(value = "姓名")
    private String userName;
    @ApiModelProperty(value = "年齡" , example = "18")
    private int age;
    @ApiModelProperty(value = "信箱地址")
    private String email;
}
