package tw.davidlee.demo.microservice.producer.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import tw.davidlee.demo.microservice.producer.entity.User;

import java.math.BigInteger;

/**
 * Service 需考慮不同Controller共用的情況
 * */
public interface UserService {
    void addUser(User user);
    IPage<User> getPage(Page<User> page , User condition);
    User getUserById(BigInteger id);
    int setUserById(User condition);
    int removeUserById(BigInteger id);
}
