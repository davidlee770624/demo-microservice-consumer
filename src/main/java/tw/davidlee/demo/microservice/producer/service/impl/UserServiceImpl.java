package tw.davidlee.demo.microservice.producer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tw.davidlee.demo.microservice.producer.entity.User;
import tw.davidlee.demo.microservice.producer.mapper.UserMapper;
import tw.davidlee.demo.microservice.producer.service.UserService;
import tw.davidlee.microservice.springmvc.exception.ServiceException;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.util.List;
import java.util.Objects;

/**
 * Service 需考慮不同Controller共用的情況
 * */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    UserMapper userMapper;

    @Override
    @Transactional( propagation= Propagation.MANDATORY  )
    public void addUser(User user) {
        // 檢查 email是否重複
        QueryWrapper<User> query = new QueryWrapper<User>();
        query.lambda().eq(User::getEmail,user.getEmail());
        List<User> list =  userMapper.selectList(query);
        if(list.size()>0)
            throw new ServiceException("請重新輸入EMAIL地址!");
        userMapper.insert(user);
    }

    @Override
    public IPage<User> getPage( Page<User> page , User condition) {
        QueryWrapper<User> query = new QueryWrapper<>();
        query.lambda().eq(  Objects.nonNull(condition.getAge()),User::getAge,condition.getAge());
        query.lambda().eq(  Objects.nonNull(condition.getUserName()),User::getUserName,condition.getUserName());
        query.lambda().eq(  Objects.nonNull(condition.getEmail()),User::getEmail,condition.getEmail());
        return userMapper.selectPage( page , query);
    }

    @Override
    @Transactional
    public User getUserById(BigInteger id) {
        return userMapper.selectById(id);
    }

    @Override
    @Transactional
    public int setUserById(User condition) {
        return userMapper.updateById(condition);
    }

    @Override
    @Transactional
    public int removeUserById(BigInteger id) {
        return userMapper.deleteById(id);
    }
}
