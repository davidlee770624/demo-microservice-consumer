package tw.davidlee.demo.microservice.producer.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import tw.davidlee.demo.microservice.producer.bo.InsertUserBO;
import tw.davidlee.demo.microservice.producer.bo.QueryUsersBO;
import tw.davidlee.demo.microservice.producer.bo.UpdateUserBO;
import tw.davidlee.demo.microservice.producer.entity.User;
import tw.davidlee.demo.microservice.producer.service.UserService;
import tw.davidlee.microservice.springmvc.http.ApiResponse;

import java.math.BigInteger;

@Slf4j
@RestController
@RequestMapping("/api/v1/")
@Api(tags ="USER資料表增刪改查")
public class UserController {

    @Autowired
    UserService userService;

    @ApiResponses(value = {@io.swagger.annotations.ApiResponse(code=200,message = "成功")})
    @ApiOperation( value= "增加 user 資料")
    @PostMapping(value = "/user" , produces = MediaType.APPLICATION_JSON_VALUE)
    public ApiResponse insertUser(@RequestBody InsertUserBO bo) {
        User user = new User();
        BeanUtils.copyProperties(bo, user);
        userService.addUser(user);
        return new ApiResponse();
    }

    @ApiResponses(value = {@io.swagger.annotations.ApiResponse(code=200,message = "成功")})
    @ApiOperation( value= "查詢所有 user 資料")
    @GetMapping(value = "/users" , produces = MediaType.APPLICATION_JSON_VALUE)
    public ApiResponse queryUsers( QueryUsersBO userBO ) {
        Page<User> page = new Page();
        BeanUtils.copyProperties(userBO,page);
        User user = new User();
        BeanUtils.copyProperties(userBO,user);
        return new ApiResponse(userService.getPage(page,user));
    }

    @ApiResponses(value = {@io.swagger.annotations.ApiResponse(code=200,message = "成功")})
    @ApiOperation( value= "查詢單一 user 資料")
    @GetMapping(value = "/user/{id}" , produces = MediaType.APPLICATION_JSON_VALUE)
    public ApiResponse queryUser(@PathVariable BigInteger id) {
        return new ApiResponse(userService.getUserById(id));
    }

    @ApiResponses(value = {@io.swagger.annotations.ApiResponse(code=200,message = "成功")})
    @ApiOperation( value= "修改單一 user 資料")
    @PutMapping(value = "/user/{id}" , produces = MediaType.APPLICATION_JSON_VALUE)
    public ApiResponse updateUser(@PathVariable BigInteger id , @RequestBody UpdateUserBO userBO) {
        User user = new User();
        BeanUtils.copyProperties(userBO,user);
        user.setId(id);
        return new ApiResponse(userService.setUserById(user));
    }

    @ApiResponses(value = {@io.swagger.annotations.ApiResponse(code=200,message = "成功")})
    @ApiOperation( value= "刪除單一 user 資料")
    @DeleteMapping(value = "/user/{id}" , produces = MediaType.APPLICATION_JSON_VALUE)
    public ApiResponse deleteUser(@PathVariable BigInteger id) {
        return new ApiResponse(userService.removeUserById(id));
    }


}
