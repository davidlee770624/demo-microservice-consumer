package tw.davidlee.demo.microservice.producer.service;

public interface RedisCacheService {
    public Object getById(String id, String name);
}
