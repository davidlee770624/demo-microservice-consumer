package tw.davidlee.demo.microservice.producer.listener;


import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import tw.davidlee.starter.rabbitmq.constant.QueueNameConstant;

@Component
@RabbitListener(queues = QueueNameConstant.NOTICE_MSG)
public class RabbitMQListener {
    @RabbitHandler
    public void receive(@Payload Object payload){
        System.out.println("接到了!");
        System.out.println(payload.toString());
    }
}
