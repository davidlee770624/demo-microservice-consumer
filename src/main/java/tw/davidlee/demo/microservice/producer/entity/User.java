package tw.davidlee.demo.microservice.producer.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.math.BigInteger;

@Data
@TableName("USER")
public class User {

    @TableId( type= IdType.AUTO )
    private BigInteger id;
    private String userName;
    private int age;
    private String email;

    @TableField(fill = FieldFill.INSERT)
    private java.time.ZonedDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private java.time.ZonedDateTime updateTime;

}
