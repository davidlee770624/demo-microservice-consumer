package tw.davidlee.demo.microservice.producer.sqlprovider;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import tw.davidlee.demo.microservice.producer.entity.User;
import org.apache.ibatis.jdbc.SQL;

public class UserSqlProvider {

    /**
     * 顯示 sql provider
     * */
    public String selectAll() {
        String tablename = "USER";
        SQL sql = new SQL();
        sql.SELECT("*").FROM(tablename);
        return sql.toString();
    }

    /**
     * 顯示 分頁器
     * */
    public String query(Page<User> page){
        String tablename = "USER";
        SQL sql = new SQL();
        sql.SELECT("*").FROM(tablename);
        return sql.toString();
    }
}
