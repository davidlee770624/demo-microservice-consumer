package tw.davidlee.demo.microservice.producer.service.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import tw.davidlee.demo.microservice.producer.service.RedisCacheService;

import java.util.HashMap;
import java.util.Map;

/**
 *  @Cacheable： 当重复使用 "相同参数" 调用方法的时候，方法本身不会被调用执行，直接从缓存中找到并返回
 *  @CachePut： 这个注释可以 "确保方法被执行" ，同时方法的返回值也被"记录到缓存"
 *  @CacheEvict: 要新增或者修改或者删除数据,那么,需要将redis中数据删除,当再次调用查询方法时,会发现redis中没有数据,
 *  需要从数据库中查找并存到缓存redis中
 *
 * **/
@Service
public class RedisCacheServiceImpl implements RedisCacheService {

    /**
     * @Cacheable未指定"key"預設採用keyGenerator()
     * Redis塞值用 'Hash'資料儲存 , key=>@Cacheable(cacheNames) , value=>Map資料
     * Map資料中的key由方法參數組成 , 例如getById(String id ,String name)中的id與name
     * */
    @Override
    @Cacheable( cacheNames="thisIsCacheNames",  sync = true)
    public Object getById(String id ,String name) {
        Map tmp = new HashMap();
        tmp.put("id",id);
        tmp.put("name",name);
        // 查看console有無讀取緩存
        System.out.println("未讀取緩存!!");
        return tmp;
    }
}
