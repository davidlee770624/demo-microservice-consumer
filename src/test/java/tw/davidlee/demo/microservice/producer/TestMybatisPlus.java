package tw.davidlee.demo.microservice.producer;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tw.davidlee.demo.microservice.producer.entity.User;
import tw.davidlee.demo.microservice.producer.mapper.UserMapper;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestMybatisPlus {

    @Resource
    UserMapper userMapper;

    /**
     * 測試 SQL PROVIDER
     * */
    @Test
    public void select(){
        User user = userMapper.selectById(1);
        System.out.println(user);

        List<User> list = userMapper.selectAll();
        System.out.println(list);
    }

    /**
     * 測試 AUTO FILL Creeate/Update欄位
     * */
    @Test
    public void insert(){
        User user = userMapper.selectById(1);
        user.setId(null);
        user.setCreateTime(null);
        user.setUpdateTime(null);
        userMapper.insert(user);
    }

    /**
     * 測試 分頁器
     * */
    @Test
    public void paginationInterceptor() {
        Page<User> page = new Page<>();
        page.setCurrent(0);
        page.setSize(3);
        userMapper.query(page);
    }
}
