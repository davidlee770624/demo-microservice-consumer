package tw.davidlee.demo.microservice.producer;

import jodd.util.StringUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tw.davidlee.demo.microservice.producer.entity.User;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestStreamApi {

    /**
     * 測試 SQL PROVIDER
     * */
    @Test
    public void optional() {
        List<User> list = new ArrayList<>();
        User user1 = new User();
        user1.setId( BigInteger.valueOf(1) );
        user1.setUserName("david");
        user1.setAge(19);
        list.add(user1);
        User user2 = new User();
        user2.setId( BigInteger.valueOf(2) );
        user2.setUserName("jerry");
        user2.setAge(18);
        list.add(user2);

        Optional<User> firstUser = list.stream()
                .filter(a -> "david".equals(a.getUserName()))
                .findFirst();

        //这样子就取到了这个对象。
        if (firstUser.isPresent()) {
            User a = firstUser.get();
            System.out.println("first User = "+a.getUserName());
        }
        //没有查到的逻辑
        else {

        }

        // 收集成List
        List<Integer> idList = list.stream()
                                    .map(User::getAge)
                                    .collect(Collectors.toList());

        // 收集成Map
        Map<BigInteger, User> map = list.stream()
                .filter(a-> StringUtil.isNotEmpty(a.getUserName()))
                .collect(Collectors.toMap(User::getId, a -> a));

    }
}
